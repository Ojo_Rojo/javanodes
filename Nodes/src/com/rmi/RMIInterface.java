package com.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

import com.nodes.Node;

public interface RMIInterface extends Remote  {
    
	Node readXML() throws RemoteException;
    String saveNode(Node node) throws RemoteException;
}
