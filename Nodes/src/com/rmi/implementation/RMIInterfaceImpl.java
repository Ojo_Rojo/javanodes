package com.rmi.implementation;

import java.rmi.RemoteException;

import com.nodes.Node;
import com.nodes.storage.implementation.XmlJaxbNodeStorage;
import com.rmi.RMIInterface;

public class RMIInterfaceImpl implements RMIInterface {
	
	public RMIInterfaceImpl() {}
	
	@Override
	public String saveNode(Node node) throws RemoteException {
		// TODO Auto-generated method stub	
		XmlJaxbNodeStorage save = new XmlJaxbNodeStorage("XmlRmiNodeStorage.xml");
		save.store(node);
		return "Successful save!!!";//prettyOutput(node,0);
	}

	@Override
	public Node readXML() throws RemoteException {
		// TODO Auto-generated method stub
		XmlJaxbNodeStorage save = new XmlJaxbNodeStorage("XmlRmiNodeStorage.xml");
		Node node = save.read();
		return node;
	}
}
