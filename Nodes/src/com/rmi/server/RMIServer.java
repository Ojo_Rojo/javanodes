package com.rmi.server;

import java.rmi.server.UnicastRemoteObject;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import com.rmi.RMIInterface;
import com.rmi.implementation.RMIInterfaceImpl;

public class RMIServer {
	  
	public static void main(String[] args) {
	        RMIInterface implementation = new RMIInterfaceImpl();

	        try {
	        	RMIInterface stub = (RMIInterface) UnicastRemoteObject.exportObject(implementation, 0);
	            Registry registry = LocateRegistry.createRegistry(8090);

	            registry.rebind("RMINodes", stub);
	        } catch (RemoteException ex) {
	            ex.printStackTrace();
	            return;
	        }
	        System.out.println( "Bound!" );
	        System.out.println( "Server will wait forever for messages." );
	    }
}
