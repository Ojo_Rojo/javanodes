package com.nodes;

import java.util.ArrayList;
import java.util.List;

import com.nodes.patterns.factory.NodeStorageFactory;
import com.nodes.patterns.proxy.MySqlNodeStorageProxy;
import com.nodes.storage.NodeStorage;
import com.nodes.storage.implementation.CompoundStorage;
import com.nodes.storage.implementation.DisplayNodeStorage;
import com.nodes.storage.implementation.MySqlJdbcNodeStorage;
import com.nodes.storage.implementation.XmlRmiNodeStorage;

public class Main {

	static String XMLInputFile = "E:\\input.xml";
	static String XMLOutputFile = "E:\\output.xml";

	public static void main(String[] args) {
		Node root = generateData();
		
		Node test = testGeneration();

		List<NodeStorage> storages = getListNodeStorage();
		
		CompoundStorage cs = new CompoundStorage(storages);
		
		cs.store(root);
		test = cs.read();
		NodeStorage dns = new  DisplayNodeStorage();
		dns.store(test);
	}

	private static List<NodeStorage> getListNodeStorage() {
		
		List<NodeStorage> storages = new ArrayList<>();
		
		storages.add(new DisplayNodeStorage()); 
		storages.add(new MySqlNodeStorageProxy("localhost", "nodes", "nodereader", "nodereaderpw")); 
		storages.add(new NodeStorageFactory().getNodeStorage("XmlDom", "XmlDomNodeStorage.xml")); 
		storages.add(new NodeStorageFactory().getNodeStorage("XmlJaxb", "XmlJaxbNodeStorage.xml")); 
		try {
			storages.add(new XmlRmiNodeStorage("localhost"));
			}
		catch(NodeStorageException e) {System.out.println("RMIServer is disabled!");}
		
		return storages;
	}

	private static Node testGeneration() {
		Node test = new Node();
		return test;
	}
	
	private static Node generateData() {
		Node child3 = Node.startBuild()
				.buildData("child3 of child2")
				.endBuild();

		Node child2 = Node.startBuild()
				.buildData("child2 of child0")
				.addChildNodes(child3)
				.endBuild();

		Node child0 = Node.startBuild()
				.buildData("child0 of root")
				.addChildNodes(child2)
				.endBuild();
	
		Node root = Node.startBuild()
				.buildData("root")
				.addChildNodes(Node.startBuild()
						.buildData("child1 of root")
						.endBuild())
				.addChildNodes(child0)
				.endBuild();
		return root;
	}
}
