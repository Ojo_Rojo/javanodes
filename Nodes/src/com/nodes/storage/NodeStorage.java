package com.nodes.storage;

import com.nodes.Node;
import com.nodes.NodeStorageException;

public interface NodeStorage {    
   
    Node read() throws NodeStorageException;
    void store(Node node) throws NodeStorageException;
}
