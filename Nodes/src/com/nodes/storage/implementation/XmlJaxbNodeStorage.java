package com.nodes.storage.implementation;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import com.nodes.Node;
import com.nodes.NodeStorageException;
import com.nodes.storage.NodeStorage;

public class XmlJaxbNodeStorage implements NodeStorage {

	private String storePlace;

	public XmlJaxbNodeStorage(String storePlace) {
		this.storePlace = storePlace;
	}

	@Override
	public void store(Node node) throws NodeStorageException {
		try {
			File file = new File(storePlace);
			JAXBContext jaxbContext = JAXBContext.newInstance(Node.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			jaxbMarshaller.marshal(node, file);
			//jaxbMarshaller.marshal(node, System.out);

		} catch (JAXBException je) {
			throw new NodeStorageException("JAXB save exeption", je);
		}
	}

	@Override
	public Node read() throws NodeStorageException {
		Node extract;
		try {
			File file = new File(storePlace);
			JAXBContext jaxbContext = JAXBContext.newInstance(Node.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			extract = (Node) jaxbUnmarshaller.unmarshal(file);

			return extract;

		} catch (JAXBException je) {
			throw new NodeStorageException("JAXB save exeption", je);
		}
	}
}
