package com.nodes.storage.implementation;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import com.nodes.Node;
import com.nodes.NodeStorageException;
import com.nodes.storage.NodeStorage;
import com.rmi.RMIInterface;

public class XmlRmiNodeStorage implements NodeStorage {

	private String server; 
	private String port = "8090";
	private RMIInterface comp;
		
	public XmlRmiNodeStorage(String server) {
		super();
		this.server = server;
		String name = "//" + this.server + ":" + this.port + "/RMINodes";
        try {
			comp = (RMIInterface) Naming.lookup(name);		
		} catch (MalformedURLException me) {
			throw new NodeStorageException("Exeption! Malformed URL has occurred!", me);
		} catch (RemoteException re) {
			throw new NodeStorageException("RemoteException: ", re);
		} catch (NotBoundException nbe) {
			throw new NodeStorageException("LookupException: ", nbe);
		}
	}

	@Override
	public Node read() throws NodeStorageException {
		try {
			return comp.readXML();
		} catch (RemoteException re) {
			throw new NodeStorageException("Exception while trying to read XML on server: ", re);
		}	
	}

	@Override
	public void store(Node node) throws NodeStorageException {
		try {
			System.out.println(comp.saveNode(node));
		} catch (RemoteException re) {
			throw new NodeStorageException("Exception while trying to seve XML on server: ", re);
		}
	}

}
