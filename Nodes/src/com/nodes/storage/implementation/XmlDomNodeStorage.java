package com.nodes.storage.implementation;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
//import java.util.ArrayList;
import java.util.Iterator;
//import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import com.nodes.Node;
import com.nodes.NodeStorageException;
import com.nodes.storage.NodeStorage;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;
import org.apache.xml.serialize.XMLSerializer;
//import org.apache.xerces.dom.ChildNode;
import org.apache.xml.serialize.OutputFormat;;

@SuppressWarnings("deprecation")
public class XmlDomNodeStorage implements NodeStorage {
	
	private String storePlace;
	Document dom;
	
	public XmlDomNodeStorage(String storePlace) { this.storePlace = storePlace; }

	public DocumentBuilder createDocument() {

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		try {
		db = dbf.newDocumentBuilder();

		//dom = db.newDocument();

		}catch(ParserConfigurationException pce) {
			throw new NodeStorageException("Error while trying to instantiate DocumentBuilder ", pce);
		}
		return db;
	}
	
	public void createDOMTree(Node n){
	
		try {
			//create the root element 
			Element rootEle = dom.createElement("node");
			dom.appendChild(rootEle);
			
			createNodeElement(n, rootEle);
			
		} catch (DOMException de) {
			throw new NodeStorageException("Error while trying to create DOMTree ", de);
		}
	}

	private void createNodeElement(Node n, Element root){

		Element nodeData = dom.createElement("data");
		Text data = dom.createTextNode(n.getData());
		
		nodeData.appendChild(data);
		root.appendChild(nodeData);

		Iterator<?> it  = n.getChildNodes().iterator();
		while(it.hasNext()) {
			Node b = (Node)it.next();
			
			Element childNodes = dom.createElement("childNodes");
			root.appendChild(childNodes);
			createNodeElement(b, childNodes);
		}
	}

	@Override
	public void store(Node node) throws NodeStorageException {

		
		dom = createDocument().newDocument();
		createDOMTree(node);
		try
		{
			//print
			OutputFormat format = new OutputFormat(dom);
			format.setIndenting(true);

			//to generate output to console use this serializer
//			XMLSerializer serializer = new XMLSerializer(System.out, format);

			//to generate a file output use fileoutputstream instead of system.out
			XMLSerializer serializer = new XMLSerializer(
			new FileOutputStream(new File(storePlace)), format);

			
			serializer.serialize(dom);

		} catch(IOException ie) {
			throw new NodeStorageException("Error while trying to write file ", ie);
		}
	}
	
	private Node readNodeElement(NodeList elemList){
		
			Node n = new Node();

			for(int i = 0; i < elemList.getLength(); i++) {
				if (elemList.item(i).getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) 
				switch (elemList.item(i).getNodeName()) {
				case "data":
					n.setData(elemList.item(i).getTextContent());
					break;
				case "childNodes":
					n.getChildNodes().add(readNodeElement(elemList.item(i).getChildNodes()));
					break;
				default:
					break;
				}
			}
			return n;
	}

	@Override
	public Node read() throws NodeStorageException {
		
		Node resultNode = new Node();
		
		try {
		
			dom = createDocument().parse(storePlace);
			dom.getDocumentElement().normalize();
		
			resultNode = readNodeElement(dom.getChildNodes().item(0).getChildNodes());
			
		
		} catch (SAXException se) {
			throw new NodeStorageException("SAXError ", se);
		} catch (IOException ie) {
			throw new NodeStorageException("Error while trying to read file ", ie);
		}
		return resultNode;
	}
	
}
