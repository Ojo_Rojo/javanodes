package com.nodes.storage.implementation;

import com.nodes.Node;
import com.nodes.NodeStorageException;
import com.nodes.storage.NodeStorage;

public class DisplayNodeStorage implements NodeStorage {

	@Override
	public Node read() throws NodeStorageException {
		throw new NodeStorageException("Can't read by display mathod!");
	}
	
	private String getTab( int tabIndex) {
		String tab = "";
		for(int i = 0; i<=tabIndex; i++) {
			tab += "\t";
    	}
		return tab;
		
	}

	private String prettyOutput(Node exempl, int tabIndex) {
		String temp = exempl.getData() + "\n";
		for(Node child:exempl.getChildNodes()) {
			temp += getTab(tabIndex) + prettyOutput(child, tabIndex+1);
    	}
		return temp;
	}
	
	@Override
	public void store(Node node) throws NodeStorageException {
		// TODO Auto-generated method stub
		System.out.print("Pretty out of " + node.getData() + ":\n");
		System.out.print(prettyOutput(node,0));
    	
	}

}
