package com.nodes.storage.implementation;

import com.nodes.Node;
import com.nodes.NodeStorageException;
import com.nodes.storage.NodeStorage;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
//import com.mysql.jdbc.Driver;

public class MySqlJdbcNodeStorage implements NodeStorage {

	private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;
    private String server, db, dbUser, dbPassword, storePlace;

    public MySqlJdbcNodeStorage(String server, String db, String dbUser, String dbPassword) {
        try {
			Class.forName("com.mysql.jdbc.Driver");	
			this.server = server;
			this.db = db;
			this.dbUser = dbUser;//"nodereader";
			this.dbPassword = dbPassword;//"nodereaderpw";
			storePlace = "jdbc:mysql://" + this.server + "/" + this.db + "?"
	                + "user=" + this.dbUser + "&password=" + this.dbPassword;
        } catch (ClassNotFoundException cnfe) {
			throw new NodeStorageException("Class not found exeption ", cnfe);
		}
    }
    
    public MySqlJdbcNodeStorage(String storePlace) {
        try {
			Class.forName("com.mysql.jdbc.Driver");	
			this.storePlace = storePlace;
        } catch (ClassNotFoundException cnfe) {
			throw new NodeStorageException("Class not found exeption ", cnfe);
		}
    }
    
    private void openConnection() { 
    	try {
    		connect = DriverManager
		        .getConnection(storePlace);
	
			statement = connect.createStatement();
    	} catch (SQLException sqle) {
        	throw new NodeStorageException("Bad address of database ", sqle);
		}
    }
   
    private void closeConnection() {
        try {
            if (resultSet != null) {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connect != null) {
                connect.close();
            }
        } catch (Exception e) {
        	throw new NodeStorageException("Close connection exeption ", e);
        }
    }
    
    private void getChildren(String pId, List<Node> pChildList) {
    	try {
			preparedStatement = connect
			        .prepareStatement("SELECT * FROM nodes.node WHERE parent_node_id = ?;");
			preparedStatement.setString(1, pId);
			ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next()) {
				Node child = new Node();
				child.setData(resultSet.getString("data"));
				
				pChildList.add(child);
			}
			resultSet.beforeFirst();
			for(Node child:pChildList) {
				if(resultSet.next())
				getChildren(resultSet.getString("id"), child.getChildNodes());
			}
			if (resultSet != null) {
                resultSet.close();
            }
			
		} catch (SQLException sqle) {
        	throw new NodeStorageException("SQL get children exeption ", sqle);
		}
    }
    
	@Override
	public Node read() throws NodeStorageException {
		// TODO Auto-generated method stub
		openConnection();
		Node result = new Node();
		try {
			preparedStatement = connect
			        .prepareStatement("SELECT * FROM nodes.node WHERE parent_node_id is null");
			resultSet = preparedStatement.executeQuery();
			//writeResultSet(resultSet);
			if(resultSet.next()) {
				result.setData(resultSet.getString("data"));
				getChildren(resultSet.getString("id"), result.getChildNodes());
			//System.out.println(resultSet.getString("data"));
			}
			
        } catch (SQLException sqle) {
        	throw new NodeStorageException("SQL read data from DB exeption ", sqle);
		} finally {
			closeConnection();
        }
		return result;
	}
	
	private void saveNode(Node node, Object pId) {
		try {
			preparedStatement = connect
			        .prepareStatement("insert into  nodes.node values (default, ?, ?)");
		
			preparedStatement.setString(1, node.getData());
			preparedStatement.setObject(2, pId);
			preparedStatement.executeUpdate();
			
			for(Node child:node.getChildNodes()) {
				saveNode(child, getParentId(node.getData()));
			}
			
        } catch (SQLException sqle) {
        	throw new NodeStorageException("SQL insert data exeption ", sqle);
		}
	}
	
	private Object getParentId(String data) {
		try {
			preparedStatement = connect
			        .prepareStatement("select id from nodes.node where data= ? ; ");
		
	            preparedStatement.setString(1, data);
	            resultSet = preparedStatement.executeQuery();
	            if(resultSet.next()) {
	            return resultSet.getString("id");}
	    } catch (SQLException sqle) {
        	throw new NodeStorageException("SQL find parent id exeption ", sqle);
		}
		return null;
	}

	@Override
	public void store(Node node) throws NodeStorageException {
		// TODO Auto-generated method stub
		openConnection();
		try {
			preparedStatement = connect
		            .prepareStatement("delete from nodes.node; ");
		            preparedStatement.executeUpdate();
		            
			saveNode(node, null);
			
		} catch (SQLException sqle) {
        	throw new NodeStorageException("SQL clean old data exeption", sqle);
		} finally {
			closeConnection();
        }
	}

}
