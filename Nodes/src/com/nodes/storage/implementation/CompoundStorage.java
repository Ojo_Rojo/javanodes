package com.nodes.storage.implementation;

import java.util.ArrayList;
import java.util.List;

import com.nodes.Node;
import com.nodes.NodeStorageException;
import com.nodes.storage.NodeStorage;

public class CompoundStorage implements NodeStorage {

	private List<NodeStorage> storages = new ArrayList<>();	
	
	public CompoundStorage(List<NodeStorage> storages) {
		this.storages = storages;
	}
	
	@Override
	public Node read() throws NodeStorageException {
		
		for(NodeStorage st: storages) {
			try {
				if (st.getClass().getSimpleName() == "DisplayNodeStorage") continue;
				return st.read();
				
			} catch (NodeStorageException nse) {
				System.out.println("Erroe while trying read with " + st.getClass().getSimpleName());	//throw new NodeStorageException("CompoundStorage exeption!", nse);
			}
		}
		throw new NodeStorageException("CompoundStorage exeption!");
	}
	
	public List<NodeStorage> getListNode() {
		return storages;
	}

	@Override
	public void store(Node node) throws NodeStorageException {
		int countError = 0;
		for(NodeStorage st: storages) {
			try {
				st.store(node);
			} catch (NodeStorageException nse) {
				System.out.println("Erroe while trying store with " + st.getClass().getSimpleName());
				countError++;
			}
		}
		if (countError == storages.size()) throw new NodeStorageException("All storages are disabled!");
	}

}
