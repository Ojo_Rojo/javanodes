package com.nodes.patterns.factory;

import com.nodes.storage.NodeStorage;
import com.nodes.storage.implementation.DisplayNodeStorage;
import com.nodes.storage.implementation.XmlDomNodeStorage;
import com.nodes.storage.implementation.XmlJaxbNodeStorage;
import com.nodes.storage.implementation.XmlRmiNodeStorage;

public class NodeStorageFactory {
	
	public NodeStorage getNodeStorage(String typeOfSave, String storePlace) {
		switch (typeOfSave){
			case "XmlDom":
				return new XmlDomNodeStorage(storePlace);
			case "XmlJaxb":
				return new XmlJaxbNodeStorage(storePlace);
//			case "MySqlJdbc":
//				return new MySqlNodeStorageProxy(storePlace);
//			case "Display":
//				return new DisplayNodeStorage();
			case "XmlRmi":
				return new XmlRmiNodeStorage(storePlace);
			default:
				return null;
		}
	}	
}
