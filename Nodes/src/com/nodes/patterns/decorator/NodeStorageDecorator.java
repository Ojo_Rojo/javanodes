package com.nodes.patterns.decorator;

import com.nodes.Node;
import com.nodes.NodeStorageException;
import com.nodes.storage.NodeStorage;

public class NodeStorageDecorator implements NodeStorage {
	
	NodeStorage component;
	
	public NodeStorageDecorator(NodeStorage c) {
		component = c;
	}

	@Override
	public Node read() throws NodeStorageException {
		System.out.println("Reading data!");
		return component.read();		
	}

	@Override
	public void store(Node node) throws NodeStorageException {
		System.out.println("Saving data!");
		component.store(node);
	}

}
