package com.nodes.patterns.proxy;

import com.nodes.Node;
import com.nodes.NodeStorageException;
import com.nodes.storage.NodeStorage;
import com.nodes.storage.implementation.MySqlJdbcNodeStorage;

public class MySqlNodeStorageProxy implements NodeStorage {
	
	private NodeStorage realMySQLNodeStorage;
    private String server, db, dbUser, dbPassword, storePlace;
	
    public MySqlNodeStorageProxy(String server, String db, String dbUser, String dbPassword) {
			this.server = server;
			this.db = db;
			this.dbUser = dbUser;//"nodereader";
			this.dbPassword = dbPassword;//"nodereaderpw";
			storePlace = "jdbc:mysql://" + this.server + "/" + this.db + "?"
	                + "user=" + this.dbUser + "&password=" + this.dbPassword;
    }
	
    public MySqlNodeStorageProxy(String storePlace) {
		this.storePlace = storePlace;
	}

	@Override
	public Node read() throws NodeStorageException {
		// TODO Auto-generated method stub
		if(realMySQLNodeStorage == null){
			realMySQLNodeStorage = new MySqlJdbcNodeStorage(storePlace);
	      }
		return realMySQLNodeStorage.read();
	}

	@Override
	public void store(Node node) throws NodeStorageException {
		// TODO Auto-generated method stub
		if(realMySQLNodeStorage == null){
			realMySQLNodeStorage = new MySqlJdbcNodeStorage(storePlace);
	      }
		realMySQLNodeStorage.store(node);
	}
}
