package com.nodes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class Node implements Serializable {
	
	private String data;
	
	private transient static NodeBuilder builder = new NodeBuilder();
		
	private List<Node> childNodes = new ArrayList<Node>();
	
	@XmlElement
	public void setData(String input) {
		data = input;
    }
    
    public List<Node> getChildNodes() {
    	return childNodes;
    }
    
    @XmlElement
	public void setChildNodes(List<Node> childNodes) {
		this.childNodes = childNodes;
	}

	public String getData() {
		return data;
	}
    
    public void showChildren() {
    	try {
    		System.out.print( data + ":\n");
    		
		
    		for(Node child:childNodes) {
    			System.out.print("\t" + child.getData() + "\n");
    		}
    	}
    	 catch (NullPointerException npe) {
			System.out.println("404");}
    }
    
    public static NodeBuilder startBuild() {
    	//NodeBuilder builder = new NodeBuilder();
    	builder.createNewNode();
		return builder;
    }
}

	class NodeBuilder {
    	protected Node node;

    	 Node getNode() { 
    		return node; 
    	}
    	  
    	public NodeBuilder createNewNode() {
    		node = new Node(); 
    		return this;
    	}

    	public NodeBuilder buildData(String data) {
    		node.setData(data);
    		return this;
    	}
    	
    	public NodeBuilder addChildNodes(Node child) {
    		node.getChildNodes().add(child);
    		return this;
    	}
    	
    	public NodeBuilder buildChildNodes(List<Node> childNodes) {
    		node.setChildNodes(childNodes);
    		return this;
    	}
    	
    	public Node endBuild() {
    		return node;		
    	}
    }